const express = require('express');
const app = express();

const PORT = 8080;

app.get('/', (req, res) => res.send('Hello GKE!'));

app.get('/api/test', (req, res) => res.json({ test: 'hello' }));
app.get('/api/test/1', (req, res) => res.json({ test: 'hello 1' }));

app.listen(PORT, console.log(`App listening on port ${PORT}`));